#!/bin/bash
# a simple daemon script that makes sure there is a running instance
# of bmore311
#
# it would be better to write a systemd

# be sure to specify the correct path to bmore311

while true; do
    PID=$(ps -eFH|awk '/bmore311tools\/bmore311.py/ {print $2}')
    if [ -z "$PID" ]; then
        /usr/local/src/bmore311tools/bmore311.py
    fi
    sleep 20
done
