# Baltimore 311

Tools for scraping Baltimore City 311 data.

## Overview

This repo contains a couple of scripts, which together can be used to harvest 311 data for the City of Baltimore. Most of the data you get is taken directly from the City's 311 web site, with very little manipulation. That site reflects phone calls, 311 app submissions, as well as submissions through the web site; for simplicity the documentation refers to them all as "calls."

## The Data

What we get back is a JSON object of 311 calls.

### Geotagging neighbhorhoods

There is one piece of data we add to the results that is not in the original object returned by the 311 site; the neighborhood where a call is registered. Many or most of the 311 calls do have lattitude and longitude points. The city is nice enough to provide shape files for neighborhood boundaries. So with a little geospatial coding, each call is tagged with a neighborhood. (see the Setup section for more on making that work)

## Goals/ Open Questions / Known issues
 - [ ] PostGreSQL backend, to replace the deafault JSON flatfile backend.
 - [ ] 311 call closure is not being captured properly. Only calls that are closed as soon as they are opened (ie: a call for information) are being recorded as closed. There's likely a way to watch for that, but it may have to be through inference (as it may be that records just disappear from the public API when set to closed).

## Usage

The main tool is [bmore311.py](bmore311.py). It is intended to use configurations in set a file, and run unattended. It requires no arugments or options. However several options are supported, see `bmore311.py -h` for the complete list.


#### Keeping it running
For some as yet undiagnosed reason, when left running for long periods of time, `bmore311.py` will stop running. To work with/around that see the [311d.sh](examples/311d.sh) example script. It is a tiny bash wrapper to watch for the `bmore311.py` process and restart it if it fails. There is most certainly a more elegant way to do this, using systemd, however this works just for limited application. `311d` will run continuously once started.

It is safe to start `311d` after you have already started `bmore311.py`.

## Installation / Setup

### Installation

These tools are all written in Python3, and have only been tested on variations of Debian Linux. It is likely that they could work in any environment, but have yet to be tested.

#### Python libraries
There isn't very much in the way of special Python libraries, with the exception of some stuff for the geocoding of neighborhoods.

Everything is tested against versions of the python libraries in the Debian repositories, but the ones from `pip` should work too. On Debian based systems, `apt install python3-gdal` should cover all the python dependencies.


### Setup

The tools require a local `config` file that tells them what backend to use (currently only JSON files, Postgres coming soon), as well as other variables involved in running the scraper. Modify [config.example](examples/config.example), and rename it simply `config` and leave it in the repo directory. The name `config` is in `.gitignore` so you will not accidentally commit details about your system.

#### Neighborhood Shape Files

Due to uncertainty around the license of the shape files, we don't inlcude them, but you can download your own copy directly [from the city's open data portal](https://data.baltimorecity.gov/Geographic/Neighborhoods/h8i5-gvdz). The configuration will need to point to the `.shp` file from the downloaded "Shapefile" zip archive.
