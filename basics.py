# -*- coding: utf-8 -*-

# pretty print for dedugging
def prettyprint(json):
    import pprint
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(json)


def converDates(date):
    import dateutil.parser
    from datetime import timezone
    from dateutil.tz import tzutc, tzlocal

    d = dateutil.parser.parse(date).astimezone(tzlocal())

    return (d.strftime('%m/%d/%Y %H:%M:%S'))  #==> '09/26/2008'


def printSummary(parsed):
    print ( 'ID: '+ parsed['id'] )
    print ( 'Service Req. ID: '+ parsed['service_request_id'] )
    print ( 'Neighborhood: '+ parsed['neighborhood'] )
    if parsed['address']:
        print ( 'Address: '+ parsed['address'] )
    if parsed['origin'] == 'API':
        print ( 'Reported through the App/Web site.' )
    elif parsed['origin'] == 'Phone':
        print ( 'Reported over the phone.' )
    t = converDates(parsed['requested_datetime'])
    print ( 'Requested: '+ t )
    print ( 'Priority: '+ parsed['priority'] )
    print ( 'Service: '+ parsed['service_name'] )
#    print ( parsed['service_notice'] )
    if parsed['description']:
        print ( 'Desc: '+ parsed['description'] )
    print ( '---------------------')
