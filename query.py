# -*- coding: utf-8 -*-

import requests

# with a json endpoint you can make simple requests
def getResults(url):
    r = requests.get(url)
    if len(r.json()) > 0:
        return r.json()
    else:
        return False
