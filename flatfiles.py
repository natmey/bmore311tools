# -*- coding: utf-8 -*-

import os, json

# stash the json results as json files
def saveJsonFile(path, report):
    identifier = report['id']
    filepath = path + identifier +'.json'
    if os.path.isfile(filepath) == False:
        with open(filepath, 'w') as outfile:
            json.dump(report, outfile)
    else:
        if report['updated_datetime'] != report['requested_datetime']:
            newj = []
            with open(filepath, 'r') as outfile:
                oldj = json.load(outfile)
                if oldj['updated_datetime'] != report['updated_datetime']:
                    print(oldj['updated_datetime'])
                    print(report['updated_datetime'])
                    print(identifier)
                    print('-------------')
