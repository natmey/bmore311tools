#!/usr/bin/python3
# -*- coding: utf-8 -*-

import requests, os, time, configparser, argparse
import basics, query, neighborhoods, flatfiles

# load the configuration
def importConf(confFile = False):
    confdir = os.getcwd()
    conf = configparser.ConfigParser()
    if not confFile:
        confFile = confdir + "/config"
    if os.path.isfile(confFile):
        conf.read(confFile)
        return conf
    else:
        print("Failed to parse "+ confFile +". Either the filename you suppplied is wrong or you must create a valid configuration file. See the README.")
        exit



def main():

    parser = argparse.ArgumentParser(description = "Tool for fetching and saving recent 311 calls.")
    parser.add_argument("-b", "--backend", help = "Override the default backend defined in the conf file.", required = False, default = "")
    parser.add_argument("-c", "--conf", help = "Use a conf file other than the default.", required = False, default = "")
    parser.add_argument("-e", "--example", help = "Pretty print a single returned 311 call's JSON object. This option only works with --dryrun", required = False, action="store_true")
    parser.add_argument("-d", "--dryrun", help = "Run the script without saving anything, instead print info to stdout. Useful for debugging.", required = False, action="store_true")
    parser.add_argument("-nd", "--nodaemon", help = "Run once, and exit, don't keep the script running as a daemon.", required = False, action = "store_true")
    parser.add_argument("-r", "--refresh", help = "Override the default refresh defined in the conf file. Takes number of seconds between refreshes", required = False, default = "")
    parser.add_argument("-s", "--summary", help = "Print a summary of one fetch of current 311 data.", required = False, action = "store_true")

    parser.add_argument("-sf", "--shapefile", help = "Override the default shape file defined in the conf file.", required = False, default = "")
    parser.add_argument("-u", "--url", help = "Override the  url for a json endpoint defined in the conf file.", required = False, default = "")

    args = parser.parse_args()

    dry = False
    if args.dryrun:
        dry = True

    confFile = False
    if args.conf:
        confFile = args.conf

    conf = importConf(confFile)

    if args.backend:
        backend = args.backend
    else:
        backend = conf['globals']['default_backend']

    if args.shapefile and os.path.isfile(args.shapefile):
        shapefile = args.shapefile
    else:
        shapefile = conf['globals']['shapefile']

    if args.url:
        url = args.url
    else:
        url = str(conf['globals']['url'])

    if args.refresh:
        refresh = int(args.refresh)
    else:
        refresh = int(conf['globals']['refreshrate'])

    def processResults(results):
        calls = []
        for r in results:
            # we are interested in neighborhoods, so we append that
            nbrhd = neighborhoods.getNeighborhood(r['lat'], r['long'], shapefile)
            if nbrhd:
                r['neighborhood'] = nbrhd
            else:
                r['neighborhood'] = ''

            calls.append(r)
        return calls

    results = query.getResults(url)
    calls = processResults(results)

    if args.summary:
        for c in calls:
            basics.printSummary(c)
        exit(0)

    if args.nodaemon and not dry:
        if (backend == 'files' ):
            for c in calls:
                flatfiles.saveJsonFile(conf['files']['path'], c)
    elif not dry:
        while True:
            for c in calls:
                if (backend == 'files' ):
                    flatfiles.saveJsonFile(conf['files']['path'], c)
            time.sleep(refresh)

    if dry:
        print("Performing a Dry Run.")
        if args.example:
            basics.prettyprint(calls[1])



if __name__ == "__main__":
    main()
